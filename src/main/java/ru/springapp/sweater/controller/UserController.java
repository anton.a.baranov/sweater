package ru.springapp.sweater.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.springapp.sweater.model.Role;
import ru.springapp.sweater.model.User;
import ru.springapp.sweater.service.UserService;

import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/user")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public String userList(Model model) {
        model.addAttribute("users", userService.findAll());
        return "userList";
    }

    @GetMapping("{user}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public String userEditForm(@PathVariable User user, Model model){
        model.addAttribute("user", user);
        model.addAttribute("roles", Role.values());
        return "userEdit";
    }
    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public String userSave(@RequestParam("userId") User user,
                           @RequestParam Map<String, String> form,
                           @RequestParam String username) {
        user.setUsername(username);
        Set<String> roles = Arrays.stream(Role.values())
                .map(Role::name)
                .collect(Collectors.toSet());
        user.getRoles().clear();
        for(String key: form.keySet()) {
            if(roles.contains(key)) {
                user.getRoles().add(Role.valueOf(key));
            }
        }
        userService.save(user);
        return "redirect:/user";
    }

    @GetMapping("delete/{user}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public String deleteUser(@PathVariable User user) {
        userService.delete(user);

        return "redirect:/user";
    }

    @GetMapping("profile")
    public String userProfile() {
        return "profile";
    }

    @PostMapping("edit")
    public String editUser(@AuthenticationPrincipal User user,
                           Model model,
                           @RequestParam("password") String password,
                           @RequestParam("email") String email) {
        List<String> messages = new ArrayList<>();

        if(password == null || password.isEmpty()) {
            messages.add("Пароль не может быть пустым");
        }
        if (messages.size() > 0) {
            model.addAttribute("messages", messages);
            return "profile";
        }
        userService.editUser(user, password, email);

        return "redirect:/main";
    }
}
