package ru.springapp.sweater.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import ru.springapp.sweater.model.Message;
import ru.springapp.sweater.model.User;
import ru.springapp.sweater.repository.MessageRepository;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static ru.springapp.sweater.controller.ControllerUtils.getErrors;

@Controller
public class MainController {
    private final MessageRepository messageRepository;

    @Value("${upload.path}")
    private String uploadPath;

    @Autowired
    public MainController(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @GetMapping("/")
    public String greeting(Model model) {
        return "greeting";
    }

    @GetMapping("/main")
    public String main(@AuthenticationPrincipal User user,
                       @RequestParam(required = false, defaultValue = "") String tag,
                       Model model) {
        model.addAttribute("messages", messageRepository.findAll());

        Iterable<Message> messages;
        if(tag != null && !tag.isEmpty()) {
            messages = messageRepository.findByTag(tag.trim());
        }
        else {
            messages = messageRepository.findAll();
        }
        model.addAttribute("messages", messages);
        model.addAttribute("tag", tag);

        return "main";
    }

    @PostMapping("/main")
    public String postMessage(@RequestParam("file") MultipartFile file,
                              @AuthenticationPrincipal User user,
                              @Valid Message message,
                              BindingResult bindingResult, // всегда перед model!
                              Model model) throws IOException {
        message.setAuthor(user);
        if(bindingResult.hasErrors()) {
            Map<String, String> errors = getErrors(bindingResult);
            model.mergeAttributes(errors);
            model.addAttribute("message", message);
        }
        else {
            if (file != null && !file.isEmpty() && !file.getOriginalFilename().isEmpty()) {
                File uploadDir = new File(uploadPath);
                if (!uploadDir.exists()) {
                    uploadDir.mkdirs();
                }
                String serverFile = UUID.randomUUID().toString() + "." + file.getOriginalFilename();
                file.transferTo(new File(uploadPath + "/" + serverFile));
                message.setFilename(serverFile);
            }
            messageRepository.save(message);
            model.addAttribute("message", null);
        }

        List<Message> messages = messageRepository.findAll();
        model.addAttribute("messages", messages);
        return "main";
    }
}
