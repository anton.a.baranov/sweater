package ru.springapp.sweater.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import ru.springapp.sweater.model.User;
import ru.springapp.sweater.model.dto.CaptchaResponse;
import ru.springapp.sweater.service.UserService;

import javax.validation.Valid;
import java.util.Collections;
import java.util.Map;

import static ru.springapp.sweater.controller.ControllerUtils.getErrors;

@Controller
public class RegistrationController {
    private static final String CAPTCHA_URL = "https://www.google.com/recaptcha/api/siteverify?secret=%s&reponse=%s";
    private final UserService userService;

    private final RestTemplate restTemplate;

    @Autowired
    public RegistrationController(UserService userService, RestTemplate restTemplate) {
        this.userService = userService;
        this.restTemplate = restTemplate;
    }

    @Value("${recaptcha.secret}")
    private String recaptchaSecret;

    @GetMapping("/registration")
    public String registration(){
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(@RequestParam("g-recaptcha-response") String recaptchaResponse,
                          @RequestParam("passwordConfirm") String passwordConfirm,
                          @Valid User user,
                          BindingResult bindingResult, Model model) {
        String captchaUrl = String.format(CAPTCHA_URL, recaptchaSecret, recaptchaResponse);
        CaptchaResponse captchaResponse = restTemplate.postForObject(captchaUrl, Collections.emptyList(), CaptchaResponse.class);

        if (!captchaResponse.isSuccess()) {
            model.addAttribute("captchaError", "Проверка не пройдена");
        }

        boolean isPasswordsNotEquals = user.getPassword() != null && !user.getPassword().equals(passwordConfirm);
        if(isPasswordsNotEquals) {
            model.addAttribute("passwordConfirmError", "Пароли не совпадают");
        }

        if(bindingResult.hasErrors() || isPasswordsNotEquals || !captchaResponse.isSuccess()) {
            Map<String, String> errors = getErrors(bindingResult);
            model.mergeAttributes(errors);
            return "registration";
        }
        if (!userService.addUser(user)) {
            model.addAttribute("usernameError", "Пользователь уже существует");
            return "registration";
        }

        return "redirect:/login";
    }

    @GetMapping("/activate/{code}")
    public String activate(Model model, @PathVariable("code") String code) {
        boolean isActivated = userService.activateUser(code);
        if(isActivated) {
            model.addAttribute("alertType", "success");
            model.addAttribute("message", "Активация прошла успешно");
        }
        else {
            model.addAttribute("alertType", "danger");
            model.addAttribute("message", "Код активации не найден");
        }
        return "login";
    }
}
