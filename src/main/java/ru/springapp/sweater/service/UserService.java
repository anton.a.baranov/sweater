package ru.springapp.sweater.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.springapp.sweater.model.Role;
import ru.springapp.sweater.model.User;
import ru.springapp.sweater.repository.UserRepository;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;
    private final MailSender mailSender;

    @Value("${local.server.name}")
    private String serverName;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, MailSender mailSender, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.mailSender = mailSender;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if(user == null) {
            throw new UsernameNotFoundException(String.format("Пользователь %s не найден", username));
        }
        return user;
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public void save(User user) {
        userRepository.save(user);
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public boolean addUser(User user) {
        User userFromDb = userRepository.findByUsername(user.getUsername());
        if(userFromDb != null) {
            return false;
        }

        user.setActive(true);
        user.setRoles(Collections.singleton(Role.USER));
        user.setActivationCode(UUID.randomUUID().toString());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);

        sendActivationEmail(user);
        return true;
    }

    public void sendActivationEmail(User user) {
        if(user.getEmail() != null && !user.getEmail().isEmpty()) {
            String message = String.format(
                "Добро пожаловать, %s\n" +
                      "Для подтверждения почтового ящика, перейдите по ссылке: %s/activate/%s",
                user.getUsername(),
                serverName,
                user.getActivationCode()

            );
            mailSender.send(user.getEmail(), "Подтвердите электронную почту", message);
        }
    }

    public boolean activateUser(String code) {
        User user = userRepository.findByActivationCode(code);
        if(user == null) {
            return false;
        }
        user.setActivationCode(null);
        userRepository.save(user);

        return true;
    }

    public void delete(User user) {
        userRepository.delete(user);
    }

    public void editUser(User user, String password, String email) {
        if(email != null && !email.isEmpty() && !email.equals(user.getEmail())) {
            user.setActivationCode(UUID.randomUUID().toString());
            sendActivationEmail(user);
        }

        user.setPassword(passwordEncoder.encode(password));
        user.setEmail(email);

        save(user);
    }
}
