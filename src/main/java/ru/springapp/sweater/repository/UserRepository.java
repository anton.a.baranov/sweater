package ru.springapp.sweater.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.springapp.sweater.model.User;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByUsername(String username);

    User findByActivationCode(String code);
}
