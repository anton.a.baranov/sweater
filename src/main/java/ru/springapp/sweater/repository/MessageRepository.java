package ru.springapp.sweater.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.springapp.sweater.model.Message;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Integer> {
    List<Message> findByTag(String tag);
}
