create sequence hibernate_sequence start 1 increment 1;

create table usr (
    id serial,
    activation_code text,
    active boolean not null,
    email text, password text,
    username text,
    primary key (id)
);

create table user_role (
    id_user integer not null,
    roles text
);

create table message (
    id serial,
    filename text,
    tag text,
    text text,
    id_user integer,
    primary key (id)
);

alter table if exists usr add constraint unique_username unique (username);
alter table if exists message add constraint fk_user foreign key (id_user) references usr;
alter table if exists user_role add constraint fk_user foreign key (id_user) references usr;